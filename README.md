# Đánh index các Driver và Pickup locations sẵn có
```
Driver.reindex
```

# Để search các Ride Offer trong 1 khoảng cách nhất định vả order theo khoảng cách tăng dần
```ruby
Driver.search '*', where: {
  locations: {
    near: { lat: lat, lon: lon },
    within: "#{params[:distance] || 30}mi"
  }
},
order: {
  _geo_distance: {
    location: "#{lat},#{lon}",
    order: 'asc',
    unit: 'mi'
  }
}
```