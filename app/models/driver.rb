class Driver < ApplicationRecord
  has_many :locations
  searchkick locations: [:pickup_location]

  def search_data
    attributes.merge(
      pickup_location: locations.map { |loc| {lat: loc.latitude, lon: loc.longitude} }
    )
  end
end
